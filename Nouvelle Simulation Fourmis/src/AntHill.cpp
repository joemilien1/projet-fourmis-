#include "AntHill.hpp"


AntHill::AntHill(unsigned int numberOfResources, bool actived, Ant::colony antHillId)
	:	Square(actived)
	,	antHillId(antHillId) 
{
	giveResources(numberOfResources);
}


Ant::colony AntHill::getAntHillId() 
{
	return antHillId;
}
