#ifndef _QUEENANT_HPP
#define _QUEENANT_HPP


#include "Ant.hpp"

class Ground;
class QueenAnt;

class ActionQueen
{
	public :
		virtual void execute(QueenAnt * queenAnt) = 0;
};


class ActionQueen1 : public ActionQueen
{
	public :
		void execute(QueenAnt * queenAnt);
};


class ActionQueen2 : public ActionQueen
{
	public :
		void execute(QueenAnt * queenAnt);
};


class ActionQueen3 : public ActionQueen
{
	public :
		void execute(QueenAnt * queenAnt);
};


class QueenAnt : public Ant
{
	private :
		unsigned int numberEggs;
		unsigned int maxNumberEggs; // limite du nombre d'oeufs que peut avoir une reine
		unsigned int numberOfResourcesAvailable ;

		ActionQueen * actionQueen;
		
	public :
		QueenAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int maxNumberEggs, Simulation * simulation);
		
		// la reine tombe enceinte si elle a suffisamment de ressources et si elle est deja enceinte elle donne naissance
		void action(); 

		// la fourmi reine ne bouge pas
		void move();

		// crée autant de fourmis que la reine avait d'oeufs
		void giveBirth(); // passer private

		// la reine crée autant d'oeufs qu'elle le peut en fonction du nombre de resources données en paramètre d'entrée 
		void getPregnant(unsigned int numberOfResourcesAvailable); // passer private

		void antSpecificsInformations();

		void doNothing();
		
		unsigned int getNumberOfResourcesAvailable() ;
};

#endif
