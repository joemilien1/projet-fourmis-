#include "Simulation.hpp"
#include "RecoltingAnt.hpp"
#include "SoldierAnt.hpp"
#include "QueenAnt.hpp"
#include "Square.hpp"
#include "Display.hpp"
#include "AntHill.hpp"
#include "json.hpp"
#include "mt19937ar.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <fstream>



int main(int argc , char *argv[])
{
	int numberOfColonies = 3;
	int numberOfTurns = 50;

	
	if (argc <= 2)
	{
		std::cout << "Veuillez donner : " << std::endl << "le nombre de tour de la simulation (unsigned int) et le nombre de colonie (unsigned int) 2,3 ou 4" << std::endl;
	}
	else
	{
		numberOfColonies = atoi(argv[2]);
		numberOfTurns = atoi(argv[1]);

		if (numberOfColonies < 2 || numberOfColonies > 4)
		{
			std::cout << "Il faut choisir un nombre de colonie entre 2,3 ou 4" << std::endl;
		}
		else
		{
			if ( numberOfTurns <= 0 ) 
			{
				std::cout << "Le nombre de tour de la simulation doit être une entier supérieur à zero" << std::endl;
			}
			else
			{			
				/*--------- Initialisation Generateur Aleatoire ----------*/
				unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
			 	init_by_array(init, length);
				/*--------------------------------------------------------*/

				// On instancie une simulation
				Simulation s = Simulation((char *)"./src/Config.json", numberOfColonies);

				// On fait tourner la simulation en effectuant n tours ou on réalise à chaque tour les actions de chaque fourmis 
				s.runSimu(numberOfTurns); 
			}
		}
	}
	return 0;
}
