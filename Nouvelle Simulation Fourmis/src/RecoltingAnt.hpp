#ifndef _RECOLTINGANT_HPP
#define _RECOLTINGANT_HPP

#include "Ant.hpp"

class Ground;
class RecoltingAnt;

class ActionRecoltingAnt
{
	public :
    	virtual void execute(RecoltingAnt *) = 0;
};


class Action1 : public ActionRecoltingAnt
{
	public :
		void execute(RecoltingAnt *); 
};

class Action2 : public ActionRecoltingAnt
{
	public :
		void execute(RecoltingAnt *);
};

class Action3 : public ActionRecoltingAnt
{
	public :
		void execute(RecoltingAnt *);
};


class RecoltingAnt : public Ant
{
	private :
		unsigned int numberOfResourcesCarried;
		unsigned int MaxNumberResourcesCanCarried; // maximum de ressources que la fourmi récoltrice peut porter
		
		ActionRecoltingAnt * actionRecoltingAnt;
		
	public :
		void antSpecificsInformations();
		
		RecoltingAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int MaxNumberResourcesCanCarried, Simulation * simulation);

		// la fourmi se déplace aléatoirement tant qu'elle a pasde ressource. Des qu'elle a une ressource elle rentre à la fourmilière 
		void move();

		// ramasse une ressource sur la case s'il y en a et si elle n'en porte pas déjà à son maximum
		void action();

		// donne le nombre de ressource que la fourmi peut prendre en fonction du nombre de ressources sur la case le nombre que la fourmi porte déjà et le maximum qu'elle peut porter 
		unsigned int howManyResourcesCanBeTaken(unsigned int numberOfResourcesAvailable);

		// met à 0 le nombre de ressources que porte la fourmi
		void resetNumberOfResourcesCarried();
		
		// met à jours le nombre de ressources portées par la fourmi
		void updateNumberOfResourcesCarried(unsigned int numberResourcesTaken);
		
		// getter
		unsigned int getNumberOfResourcesCarried();
		
		void doNothing();
};

#endif