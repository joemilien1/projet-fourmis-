#include "Ant.hpp"
#include "Ground.hpp"
#include "Square.hpp"
#include "Simulation.hpp"
#include "mt19937ar.hpp"

#include <iostream>
#include <ctime>        
#include <cstdlib> 
#include <algorithm>  


unsigned int Ant::antCounter = 0;
unsigned int Ant::antIdCounter = 0;

unsigned int adultAgeAnt = 5;
float deathRateAnt = 0.01;


unsigned int Ant::getIdAnt()
{
	return antId;
}


Ant::Ant(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, Simulation * simulation)
	:	simulation(simulation)
	,	ground(ground)
	,	antAge(0)
	,	adult(false)
	,	life(life)
	,	xAntHill(xAntHill)
	,	yAntHill(yAntHill)
	,	colonyId(colonyId)	
{
	antCounter++;
	antIdCounter++;
	antId = antIdCounter;
	positionVector.push_back(xAntHill);
	positionVector.push_back(yAntHill);
	
	square = whatIsMySquare() ;
	square->addAnt(this);
	
	
	std::vector<int> vect1{0, 1};
	std::vector<int> vect2{1, 0};
	std::vector<int> vect3{0, -1};
	std::vector<int> vect4{-1, 0};

	vectorsPossibleMovements.push_back(vect1);
	vectorsPossibleMovements.push_back(vect2);
	vectorsPossibleMovements.push_back(vect3);
	vectorsPossibleMovements.push_back(vect4);
}


Ant::Ant()
{
}


//creer une fonction qui verifie le placement a l'instanciation de la fourmi 
std::vector<unsigned int> Ant::getPositionVector()
{
	return positionVector;
}


Ant::colony Ant::getColonyId()
{
	return colonyId;
}


void Ant::antInformations()
{
	std::cout 	<< "I'm an the ant n°" << antId << " I belong to the colony n°" << colonyId 
				<< "I'm on the Square n°" << square->getIdSquare() << " and my life : " << life << std::endl;
}


void Ant::updateAnt()
{
	antAge++;
	double p;
	if (antAge > adultAgeAnt)
	{
		adult = true;
	}
	if (adult) 
	{
		p = genrand_real2();
		if (p < deathRateAnt) 
		{
			life -= 1;
		}
	}
	
	if (life <= 0)
	{
		deathAnt();
	}
}


Ant::~Ant()
{	
}


void Ant::antSpecificsInformations()
{
	std::cout << "test" << std::endl;
}


void Ant::deathAnt()
{
	square->removeAnt(this);
	simulation->removeAntFromSimulation(this);
}


//remplacer le int par un generics
std::vector<unsigned int> vectorsSum(std::vector<unsigned int> vector1, std::vector<int> vector2) 
{
	std::vector<unsigned int> newVector;
	if (vector1.size() == vector2.size())
	{
		for (unsigned int i = 0; i < vector1.size(); i++) 
		{
			newVector.push_back(vector1[i] + (unsigned int)vector2[i]);
		}
	}
	else
	{
		// faire un traitement erreur ici 
	}
	return newVector;
}


void Ant::updatePositionVector()
{
	positionVector = vectorsSum(positionVector, vectorIntentionDeplacement);
}


void Ant::leaveSquare()
{
	ground->getSquare(positionVector)->removeAnt(this);
}


void Ant::enterSquare()
{
	ground->getSquare(positionVector)->addAnt(this);
}


void Ant::randomMove() 
{
	std::random_shuffle(vectorsPossibleMovements.begin(), vectorsPossibleMovements.end()); // on melange le vecteur des deplacements possibles
	choseDirection();
	leaveSquare();
	updatePositionVector();
	enterSquare();
	square = whatIsMySquare() ;
}


void Ant::choseDirection()
{
	int n = -1;
	bool lookingFor = true;
	std::vector<unsigned int> squareToGo;
	
	while (lookingFor) 
	{	
		n++;
		squareToGo = vectorsSum(positionVector, vectorsPossibleMovements[n]);
		lookingFor = !(ground->isValid(squareToGo)); // si le deplacement est valide on arrête de regarder 
	}
	vectorIntentionDeplacement = vectorsPossibleMovements[n];
}



bool Ant::isRightDirection(int xDestination, int yDestination, std::vector<int> vectorPossibleMovement)
{	

	if (vectorPossibleMovement[0] == 0) // on se deplace sur les y 
	{
		if (yDestination - (int)positionVector[1] > 0) // il faut augmenter y pour rejoindre yDestination
		{
			if (vectorPossibleMovement[1] > 0) 
			{
				return true;
			}
		}
		else
		{
			if (yDestination - (int)positionVector[1] < 0) // il faut diminuer y pour rejoindre yDestination
			// on est obligé decaster sinon c'esttjr faut car on a un unsigned int 
			{
				if (vectorPossibleMovement[1] < 0)
				{
					return true;
				}
			}
		}
	}
	else
	{

		if (xDestination - (int)positionVector[0] > 0) // il faut augmenter x pour rejoindre xDestination
		{
			if (vectorPossibleMovement[0] > 0) 
			{
				return true;
			}
		}	
		else
		{
			if (xDestination - (int)positionVector[0] < 0) // il faut diminuer x pour rejoindre xDestination
			{
				if (vectorPossibleMovement[0] < 0)
				{
					return true;
				}
			}
		}		
	}	
	return false;
}


void Ant::goSomewhere(unsigned int xDestination, unsigned int yDestination)
{
	unsigned int n = 0;
	bool lookingFor = true;
	
	std::random_shuffle(vectorsPossibleMovements.begin(), vectorsPossibleMovements.end());
	
	while (lookingFor && n < vectorsPossibleMovements.size()) 
	{
		lookingFor = !(this->isRightDirection((int)xDestination, (int)yDestination, vectorsPossibleMovements[n])); 
		n++;
		// pas besoin de checker si le deplacement est valide car si on passe isrightdirection on se rapproche d'un objet qui est dans le terrain on ne va pas vers les bords			
	}
	
	if ( n == vectorsPossibleMovements.size() && lookingFor) 
	{
		std::vector<int> auxiliaryVector{0, 0};
		vectorIntentionDeplacement = auxiliaryVector;
	}
	else
	{
		n--; 
		vectorIntentionDeplacement = vectorsPossibleMovements[n];
		leaveSquare();
		updatePositionVector();
		enterSquare();
	}	
	square = whatIsMySquare() ;
}


void Ant::move()
{
}


void Ant::action()
{
}


Square * Ant::whatIsMySquare() 
{
	return ground->getSquare(positionVector);
}


void Ant::getHurt(int hitStrength) 
{
	std::cout << "fight" << std::endl;
	life -= hitStrength;
}


void Ant::hurtAnt(int hitStrength)
{
	life -= hitStrength;
}


int Ant::getLife() 
{
	return life;
}
