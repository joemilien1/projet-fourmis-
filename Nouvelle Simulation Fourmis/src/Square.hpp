#ifndef _SQUARE_HPP
#define _SQUARE_HPP

#include <vector>

class Ant;

class Square {
	private :
		bool actived;
		std::vector<Ant *> antVector; // vecteur qui contient tout les fourmis présentent sur la case 
		unsigned int idSquare;
		unsigned int numberOfResources;
	
	public :
		Square(bool actived); 
		virtual ~Square(); // si on ne met pas le virtual on ne peut pas faire de dynamiccast 
	
		// vérifie que la case est activé. Le terrain de la simulation est un rectangle mais si on voulait en faire un rond il suffirait de n'activer que certaines cases lors de la création du terrain. De même on pourrait penser que sur le terrain il y a des obstacles qui empêchent les fourmis d'aller sur la case ( un arbre par exemple )
		bool isActived();

		// ajoute la fourmi fournie en paramètre dans antVector ( donc ajoute la fourmi à la case )
		void addAnt(Ant *ant);

		// retire la fourmi fournie en paramètre de antVector ( donc ajoute la fourmi à la case )
		void removeAnt(Ant * ant);

		// getter
		unsigned int getIdSquare();
		std::vector<Ant *> getAntsVector();
		
		// return true s'il n'y a aucune fourmi dans la case
		bool isEmpty();

		// retire des ressources de la case
		void takeResources(unsigned int numberOfResourcesTaken);

		// donne des ressources à la case
		void giveResources(unsigned int numberOfResourcesGiven);

		// retourne le nombre de ressources disponibles sur la case
		unsigned int numberResourcesAvailable();

		// fonction utilisée lors de la génération du terrain. Elle fournit un nombre aléatoire de ressources sur la case 
		void howManyResources();
		
		// compte le nombre de cases générées par la simulation
		static unsigned int squareCounter;
};

#endif
