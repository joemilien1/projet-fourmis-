#include <iostream>
#include <algorithm>  
#include "SoldierAnt.hpp"
#include "Square.hpp"

SoldierAnt::SoldierAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int strength, Simulation * simulation)
	:	Ant(ground, colonyId, xAntHill, yAntHill, life, simulation)
	,	strength(strength) 
{
}


void SoldierAnt::antSpecificsInformations() 
{
	std::cout << "I'm an ant Soldier" << std::endl;
}


std::vector<Ant *> SoldierAnt::detectingEnemies() 
{
	std::vector<Ant *> antsVector = square->getAntsVector();
	std::vector<Ant *> enemiesAntsVector;
	
	for (unsigned int i = 0; i < antsVector.size(); i++)
	{
		if (antsVector[i]->getColonyId() != colonyId)
		{
			enemiesAntsVector.push_back(antsVector[i]);
		}
	}
	std::random_shuffle (enemiesAntsVector.begin(), enemiesAntsVector.end());
	
	return enemiesAntsVector;
}


void SoldierAnt::action()
{
	
	unsigned int n = 0;
	std::vector<Ant *> enemiesAntsVector = detectingEnemies();
	
	if (enemiesAntsVector.size() != 0)
	{
		while (n < enemiesAntsVector.size() && enemiesAntsVector[n]->getLife() > 0)
		{
			n++; 
		}
		enemiesAntsVector[0]->hurtAnt(1);
	    // la fourmi ciblé est blessé par la force de ma fourmi 
	}
}


void SoldierAnt::move()
{
	if (adult)
	{
		randomMove();
	}
}
