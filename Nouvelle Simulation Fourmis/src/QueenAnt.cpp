#include "QueenAnt.hpp"
#include "Ground.hpp"
#include "Square.hpp"
#include "Simulation.hpp"


QueenAnt::QueenAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int maxNumberEggs, Simulation * simulation)
	:	Ant(ground, colonyId, xAntHill, yAntHill, life, simulation)
	,	numberEggs(0)
	,	maxNumberEggs(maxNumberEggs)  
{
	numberOfResourcesAvailable = square->numberResourcesAvailable() ;
}


void QueenAnt::move()
{
	// la reine est statique
}


void QueenAnt::antSpecificsInformations()
{
	std::cout << "I'm a QueenAnt and I have " << numberEggs << " eggs" << std::endl;
}


unsigned int QueenAnt::getNumberOfResourcesAvailable()
{
	return numberOfResourcesAvailable ;
}


void ActionQueen1::execute(QueenAnt * queenAnt)
{
	queenAnt->giveBirth();
}


void ActionQueen2::execute(QueenAnt * queenAnt )
{
	unsigned int numberOfResourcesAvailable = queenAnt->getNumberOfResourcesAvailable() ;
	queenAnt->getPregnant(numberOfResourcesAvailable);
	
}


void ActionQueen3::execute(QueenAnt * queenAnt)
{
	queenAnt->doNothing();
}


void QueenAnt::action()
{
	ActionQueen1 * actionQueen1 = new ActionQueen1();
	ActionQueen2 * actionQueen2 = new ActionQueen2();
	ActionQueen3 * actionQueen3 = new ActionQueen3();
	
	actionQueen = actionQueen3 ;
	unsigned int numberOfResourcesAvailable = square->numberResourcesAvailable();
	
	if (numberEggs != 0)
	{
		actionQueen = actionQueen1 ;
	}
	else
	{
		if (numberOfResourcesAvailable > 0)
		{
			actionQueen = actionQueen2 ;
		}
	}
	actionQueen->execute(this);
}


void QueenAnt::giveBirth()
{
	for (unsigned int i = 0; i < numberEggs; i++) 
	{
		simulation->addAntToTheSimulation(colonyId, xAntHill, yAntHill);
	}
	numberEggs = 0;
}


void QueenAnt::getPregnant(unsigned int numberOfResourcesAvailable)
{
	if (numberOfResourcesAvailable >= maxNumberEggs)
	{
		numberEggs = maxNumberEggs; 
	}
	else
	{
		numberEggs = numberOfResourcesAvailable;
	}
	square->takeResources(numberEggs);
	numberOfResourcesAvailable = square->numberResourcesAvailable() ;
}

void QueenAnt::doNothing()
{

}
