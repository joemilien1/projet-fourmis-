#ifndef _ANT_HPP
#define _ANT_HPP

#include <vector>

class Ground;
class Square;
class Simulation;

class Ant {
	
	public :
		static unsigned int antCounter; // compteur pour compter le nombre de fourmis en vie dans la simulation en cours
		static unsigned int antIdCounter; // compteur pour compter le nombre total de fourmis générer pendant toute la simulation 
		enum colony {colony1, colony2, colony3, colony4}; // la simulation peut générer jusqu'à 4 colonies différentes
		std::vector<std::vector<int> > vectorsPossibleMovements; // vecteur qui contient tout les mouvements possibles d'une fourmi


	protected :
		Simulation * simulation; //la fourmi est consciente qu'elle est dans une simulation. ce pointeur sert lorsque la fourmi meurt elle se retire elle même de la simulation 
		Ground * ground; // pointeur vers le terrain de la simulation 
		Square * square ;

		// par convention on mettra x puis y dans les vecteur de position et déplacement 
		std::vector<unsigned int> positionVector; // donne la position de la fourmi sur le terrain
		std::vector<int> vectorIntentionDeplacement; // permet de stocker en memoire le deplacement que va effectuer la fourmi 
		unsigned int antId;
		unsigned int antAge;
		bool adult;
		int life; 

		// coordonnées de la colonie de la fourmi
		unsigned int xAntHill;
		unsigned int yAntHill;

		colony colonyId;

		
	public :
		Ant();

		// constructeur pour n'importe qu'elle fourmi 
		Ant(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, Simulation * simulation);
		~Ant(); // mettre un virtual cf : https://cpp.developpez.com/faq/cpp/?page=Les-destructeurs
	
		// renvoie le carré sur lequel est situé la fourmi 
		Square * whatIsMySquare();

		// les différents getter des paramètres de la fourmi 
		unsigned int getIdAnt();
		int getLife();
		std::vector<unsigned int> getPositionVector();
		colony getColonyId();
		
		// la fourmi choisi ou elle va aller elle met à jour "vectorIntentionDeplacement"
		void choseDirection();

		// la fourmi entre dans une case en fonction de son positionVector
		void enterSquare();

		// la fourmi quitte la case ou elle est 
		void leaveSquare();

		// la fourmi se déplace de maniere aleatoire sur une case 
		void randomMove();

		void updatePositionVector();

		// met à jour la fourmi (vieillesse) et tue la fourmi si les points de vie de cette dernière sont en dessous de 0
		void updateAnt();

		// retire la fourmi de sa case et de la simulation
		void deathAnt();
		
		// fait subir à la fourmi des dommages. Réduction de sa vie en fonction de la force de frappe ( paramètre d'entrée )
		void getHurt(int hitStrength);
		
		// blesse une autre fourmi en fonction de la force de frappe ( paramètre d'entrée )
		void hurtAnt(int hitStrength);

		// la fourmi avance d'une case vers une destination spécifique ( celle donnée en paramètre d'entrée )
		void goSomewhere(unsigned int xDestination, unsigned int yDestination);

		// return true si la le vectorIntentionDeplacement fait que la fourmi se deplacera vers le point voulu 
		bool isRightDirection(int xDestination, int yDestination, std::vector<int> newVectorIntentionDeplacement);
		

		// fonctions ovveridées
		void antInformations(); // donne les informations générales de la fourmi
		virtual void antSpecificsInformations(); // donne des informations en fonction du type de la fourmis 
		virtual void move();
		virtual void action(); 		
};

#endif
