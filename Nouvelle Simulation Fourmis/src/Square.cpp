#include "Square.hpp"
#include <iostream>
#include "Ant.hpp"
#include "mt19937ar.hpp"


unsigned int Square::squareCounter = 0;

Square::Square(bool actived) 
	:	actived(actived)
{
	howManyResources();
	idSquare = squareCounter;
	squareCounter++;
}


void Square::howManyResources()
{
	double p = genrand_real2();
	
	if (p < 0.35)
	{
		if (p < 0.15)
		{
			numberOfResources = 5;
		}
		else
		{
			numberOfResources = 3;
		}
	}
	else
	{
		numberOfResources = 0;
	}
}


Square::~Square()
{
}

bool Square::isActived()
{
	return actived;
}


void Square::addAnt(Ant *ant) 
{
	antVector.push_back(ant);
}


void Square::removeAnt(Ant * ant)
{
	unsigned int antId = ant->getIdAnt();
	for (unsigned int i = 0; i < antVector.size(); i++)
	{
		if (antVector[i]->getIdAnt() == antId)
		{
			antVector.erase(antVector.begin() + i);
		}
	}
}


unsigned int Square::getIdSquare()
{
	return idSquare;
}

std::vector<Ant *> Square::getAntsVector() 
{
	return antVector;
}


void Square::takeResources(unsigned int numberOfResourcesTaken) 
{
	if (numberOfResourcesTaken <= numberOfResources)
	{
		numberOfResources -= numberOfResourcesTaken;
	}
}


void Square::giveResources(unsigned int numberOfResourcesGiven) 
{
	numberOfResources += numberOfResourcesGiven;
}		


unsigned int Square::numberResourcesAvailable() 
{
	return numberOfResources;
}


bool Square::isEmpty()
{
	if (antVector.size() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}