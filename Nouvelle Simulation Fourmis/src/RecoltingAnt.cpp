#include "RecoltingAnt.hpp"
#include "Square.hpp"
#include "Ground.hpp"

#include <iostream>


RecoltingAnt::RecoltingAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int MaxNumberResourcesCanCarried, Simulation * simulation) 
	:	Ant(ground, colonyId, xAntHill, yAntHill, life, simulation)
	,	numberOfResourcesCarried(0)
	,	MaxNumberResourcesCanCarried(MaxNumberResourcesCanCarried)
{
}


void RecoltingAnt::antSpecificsInformations() 
{
	std::cout << "I'm an ant Recolting and I carry " << numberOfResourcesCarried << " ressources on me" << std::endl;
}


void RecoltingAnt::move()
{
	if (adult)
	{
		if (numberOfResourcesCarried == 0)
		{
			randomMove();
		}
		else
		{
			goSomewhere(xAntHill, yAntHill);
		}
	}
}

/*
void RecoltingAnt::action()
{
	Square * square = whatIsMySquare();
	unsigned int numberResourcesAvailable = 0;
	unsigned int numberResourcesTaken = 0;

	numberResourcesAvailable = square->numberResourcesAvailable(); 
	
	if (isAntHill(square))
	{
		if (numberOfResourcesCarried > 0)
		{
			square->giveResources( numberOfResourcesCarried);
			numberOfResourcesCarried = 0;
		}
	}	
	else
	{
		if (numberResourcesAvailable > 0)
		{
			numberResourcesTaken = howManyResourcesCanBeTaken(numberResourcesAvailable);
			square->takeResources(numberResourcesTaken);
			numberOfResourcesCarried += numberResourcesTaken;
		}
	}	
}
*/


unsigned int RecoltingAnt::getNumberOfResourcesCarried()
{
	return numberOfResourcesCarried;
}


void RecoltingAnt::resetNumberOfResourcesCarried() 
{
	numberOfResourcesCarried = 0;
}


void Action1::execute(RecoltingAnt * recoltingAnt)
{
	Square * square = recoltingAnt->whatIsMySquare();
	
	unsigned int numberOfResourcesCarried = recoltingAnt->getNumberOfResourcesCarried();
	
	square->giveResources(numberOfResourcesCarried);
	recoltingAnt->resetNumberOfResourcesCarried();
	
	//std::cout << "traitement1" << std::endl;
}


void Action2::execute(RecoltingAnt * recoltingAnt)
{
	Square * square = recoltingAnt->whatIsMySquare() ;
	
	unsigned int numberResourcesAvailable = square->numberResourcesAvailable(); 
	unsigned int numberResourcesTaken = recoltingAnt->howManyResourcesCanBeTaken(numberResourcesAvailable); 
	
	square->takeResources(numberResourcesTaken);
	recoltingAnt->updateNumberOfResourcesCarried(numberResourcesTaken);
	
	//std::cout << "traitement2" << std::endl;
}


void Action3::execute(RecoltingAnt * recoltingAnt)
{
	recoltingAnt->doNothing();
	// std::cout << "nothing happend" << std::endl;
}


void RecoltingAnt::updateNumberOfResourcesCarried(unsigned int numberResourcesTaken)
{
	numberOfResourcesCarried += numberResourcesTaken;
}


void RecoltingAnt::action()
{
	unsigned int numberResourcesAvailable = square->numberResourcesAvailable(); 
	Action3 * action3 = new Action3();
	
	actionRecoltingAnt = action3;
	
	if (isAntHill(square))
	{
		if (numberOfResourcesCarried > 0)
		{
			actionRecoltingAnt = new Action1(); 
		}
	}	
	else
	{
		if (numberResourcesAvailable > 0)
		{
			actionRecoltingAnt = new Action2(); 
		}
	}	
	actionRecoltingAnt->execute(this);
}


unsigned int RecoltingAnt::howManyResourcesCanBeTaken(unsigned int numberOfResourcesAvailable)
{
	unsigned int numberResourcesCanBeTaken = 0; 
	//nbr de ressources qui peuvent être prise à la case
	unsigned int numberResourcesCanTake = MaxNumberResourcesCanCarried - numberOfResourcesCarried; 
	//nbr de ressources que la fourmi peut encore ramasser 
	
	if (numberResourcesCanTake > 0) 
	{
		if (numberOfResourcesAvailable >= numberResourcesCanTake)
		{
			numberResourcesCanBeTaken = numberResourcesCanTake;
		}
		else
		{
			numberResourcesCanBeTaken = numberOfResourcesAvailable;
		}
	}
	return numberResourcesCanBeTaken;
}


void RecoltingAnt::doNothing()
{
}
