#include "Square.hpp"
#include "Ant.hpp"

class AntHill : public Square
{
	private :
		Ant::colony antHillId;
	
	public : 
		AntHill(unsigned int numberOfResources, bool actived, Ant::colony antHillId);

		// getter
		Ant::colony getAntHillId();
};


// struct qui contient toutes les informations necessaires à la création d'une fourmilière 
struct ConstructAntHill 
{
	unsigned int xAntHill; 
	unsigned int yAntHill; 
	Ant::colony colonyId; 
	unsigned int numberRessourcesInAntHill;
};
