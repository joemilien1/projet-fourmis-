#ifndef _SIMULATION_HPP
#define _SIMULATION_HPP

#include "Ground.hpp"
#include <iostream>
#include <vector>
#include "Ant.hpp"

struct ConstructAntHill ;

class Simulation {
	private : 
		Ground * ground; // terrain de la simulation ou les fourmis se déplacent
		std::vector<Ant *> antList; // vecteur qui contient toutes les fourmis de la simulation 
		
		unsigned int lifeRecoltingAnt;
		unsigned int lifeSoldierAnt;
		unsigned int lifeQueenAnt;
		unsigned int MaxNumberResourcesCanCarried;
		unsigned int maxNumberEggs;
		unsigned int soldierStrength;

		// Informations pour l'export
		unsigned int nombreDeFourmis;
		
	public :
		Simulation(char * nomFichier, unsigned int numberOfColonies);

		// crée une fourmi en fonction du type de fourmi qu'on lui donne 
		Ant * FactoryAnt(std::string antType, Ant::colony colonyId, unsigned int xAntHill, unsigned int yAntHill ) ;

		// Lance la simulation et effectue les ations des fourmis pour chaque tour ( le nombre de tour étant donée en paramètre d'entrée )  
		void runSimu(int n);

		// Ajoute une fourmi au vecteur qui contient toutes les fourmis de la simulation
		void addAntToTheSimulation(Ant::colony colonyId, unsigned int xAntHill, unsigned int yAntHill);

		// retire la fourmi donnée en paramètre dans le vecteur qui contient toutes les fourmis de la simulation 
		void removeAntFromSimulation(Ant * ant);
		
		//
		void exportInfo(char * nomFichier);
		
		ConstructAntHill setAntHill( unsigned int xAntHill, unsigned int yAntHill, Ant::colony colonyId, unsigned int numberRessourcesInAntHill);
};

#endif