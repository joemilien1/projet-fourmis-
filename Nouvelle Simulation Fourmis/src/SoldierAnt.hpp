#ifndef _SOLDIERANT_HPP
#define _SOLDIERANT_HPP

#include "Ant.hpp"

class SoldierAnt : public Ant
{
	private :
		int strength; // dommmage que la fourmi inflige au fourmi ennemis
		
	public :
		SoldierAnt(Ground * ground, colony colonyId, unsigned int xAntHill, unsigned int yAntHill, int life, unsigned int strength, Simulation * simulation);
		
		void antSpecificsInformations();

		// renvoie un vecteur qui contient toutes les fourmis ennemies sur une case
		std::vector<Ant *> detectingEnemies();

		// la fourmi soldat se déplace toujours de manière aléatoire
		void move();

		// detecte les fourmis ennemis et les attaque 
		void action();
};

#endif