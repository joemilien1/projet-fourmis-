#ifndef _GROUND_HPP
#define _GROUND_HPP

#include <vector>

class Square;
class Resources;
struct ConstructAntHill;


class Ground {

	private :
		static Ground * instance; // 
		unsigned int height_Ground; // la hauteur du terrain ( coordonnée y )
		unsigned int length_Ground; // la longueur du terrain ( coordonnée x )
		std::vector<std::vector<Square *> > ground; // vecteur de vecteur ( = matrice ) qui contient toutes les cases de la simulation 
		Ground(unsigned int length_Ground, unsigned int height_Ground, std::vector<ConstructAntHill> constructAntHillVector); // constructeur 

	public :	
		// prend des coordonnées et renvoie true si les coordonnées sont dans le terrain
		bool isValid(std::vector<unsigned int> vectorPosition);

		// getters
		int getLengthGround(); 
		int getHeightGround();
		Square * getSquare(std::vector<unsigned int> vectorPosition);
		Square * getSquareWithCoordinates(unsigned int x, unsigned int y); // cette fonction evite de créer un vecteur position on donne directement les coordonnées de la case 	
		std::vector<std::vector<Square *> > getGround();
		static Ground * getInstance(unsigned int length_Ground, unsigned int height_Ground, std::vector<ConstructAntHill> constructAntHillVector);	
};

// renvoie true si la case est une fourmilière
bool isAntHill(Square * square);


#endif
