#ifndef _DISPLAY_HPP
#define _DISPLAY_HPP

class Ant;
class AntHill;
class Ground;
class Square;

// affiche le terrain de la simulation c'est à dire toutes les cases ainsi que les ressources et fourmis dessus 
void displaySimulation(unsigned int length_Ground, unsigned int height_Ground, Ground * ground);

// affiche une fourmilière ( distinguée par couleur en fonction de son identitée )
void displayAntHill(Square * square);

// affiche le nombre de ressources sur une case s'il y en a 
void displaySquare(Square * square);

// affiche le nombre de fourmi présent sur une case 
void displayAnt(Ant * ant);

#endif