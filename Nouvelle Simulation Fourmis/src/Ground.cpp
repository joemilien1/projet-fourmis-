#include "RecoltingAnt.hpp"
#include "SoldierAnt.hpp"
#include "Square.hpp"
#include "AntHill.hpp"
#include "mt19937ar.hpp"

#include "Ground.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <ctime>       
#include <cstdlib>  

Ground * Ground::instance = NULL;

Ground * Ground::getInstance(unsigned int length_Ground, unsigned int height_Ground, std::vector<ConstructAntHill> constructAntHillVector)
{
	if (instance == nullptr)
	{
		instance = new Ground(length_Ground, height_Ground, constructAntHillVector);
	}
	return instance;
}


std::vector<std::vector<Square *> >  Ground::getGround() 
{ 
	return ground; 
} 

bool isAntHill(Square * square)
{
	AntHill * antHill = dynamic_cast<AntHill *>(square);
	
	if (antHill != nullptr)
	{
		return true;
	}
	return false;
}

//vector<vector<int> >   v2;
//Note the space between <int> and the second >. If you have them next to each other, as in >>,the compiler interprets it an operator and flags it as an error. / 

Ground::Ground( unsigned int length_Ground, unsigned int height_Ground, std::vector<ConstructAntHill> constructAntHillVector)
	:	height_Ground(height_Ground)
	,	length_Ground(length_Ground)	
{
	bool already;
	Square * squareX = nullptr;
	std::vector<Square *> squareY;
	
	unsigned int sizeConstructAntHillVector = constructAntHillVector.size(); // nombre de fourmilieres
	
	for (unsigned int i = 0; i < height_Ground; i++) // par convention on mettra la hauteur pour l'axe des y 
	{		
		for (unsigned int j = 0; j < length_Ground; j++) 
		{
			already = false;
			
			for (unsigned int n = 0; n < sizeConstructAntHillVector; n++) 
			{
				if (j == constructAntHillVector[n].xAntHill && i == constructAntHillVector[n].yAntHill)
				{
					squareX = new AntHill(constructAntHillVector[n].numberRessourcesInAntHill, true, constructAntHillVector[n].colonyId);
					squareY.push_back(squareX);
					already = true;
				}
				else
				{
					if (!already)
					{
						if (n == (sizeConstructAntHillVector - 1))
						{
							squareX = new Square(true);
							squareY.push_back(squareX);
						}
					}
				}	
			}				
		}
		ground.push_back(squareY);
		squareY.erase(squareY.begin(), squareY.end());
	}
}


int Ground::getLengthGround()
{
	return length_Ground;
}


int Ground::getHeightGround()
{
	return height_Ground;
}


Square * Ground::getSquare(std::vector<unsigned int> vectorPosition)
{
	unsigned int x = vectorPosition[0];
	unsigned int y = vectorPosition[1];

	return ground[y][x];
}


Square * Ground::getSquareWithCoordinates(unsigned int x , unsigned int y) 
{
	return ground[y][x];
}
	

//rajouter un control pour pas avoir trop de fourmi sur la même case ? 
bool Ground::isValid(std::vector<unsigned int> vectorPosition) 
{
	unsigned int x = vectorPosition[0];
	unsigned int y = vectorPosition[1];
	
	bool res = false;
	
	if (x < length_Ground && y < height_Ground)
	{
		res = getSquare(vectorPosition)->isActived();
	}
	
	return res;
}