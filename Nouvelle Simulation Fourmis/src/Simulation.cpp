#include "Simulation.hpp"
#include "RecoltingAnt.hpp"
#include "SoldierAnt.hpp"
#include "QueenAnt.hpp"
#include "Square.hpp"
#include "Display.hpp"
#include "AntHill.hpp"
#include "json.hpp"
#include "mt19937ar.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <fstream>

float birthRateSoldierAnt = 0.2;
float birthRateRecoltingAnt = 0.8;

unsigned int lifeRecoltingAnt_ = 2;
unsigned int lifeSoldierAnt_ = 2;
unsigned int MaxNumberResourcesCanCarried_ = 3;
unsigned int soldierStrength_ = 1;





Simulation::Simulation(char * nomFichier , unsigned int numberOfColonies)
{
	std::ifstream file(nomFichier);
	nlohmann::json object;
	file >> object;

	std::vector<ConstructAntHill> ConstructAntHillVector;
	for (unsigned int i = 0; i < numberOfColonies; ++i)
	{
		unsigned int numberRessources = object["AntHill"][i]["numberRessources"];
	    unsigned int xAntHill = object["AntHill"][i]["xAntHill"];
	    unsigned int yAntHill = object["AntHill"][i]["yAntHill"];
	    Ant::colony colonyId = object["AntHill"][i]["idColony"];
		ConstructAntHillVector.push_back(setAntHill(xAntHill, yAntHill, colonyId , numberRessources));
	}

	lifeQueenAnt = object["QueenAnt"][0]["life"];
	maxNumberEggs = object["QueenAnt"][0]["maxNumberEggs"];
	lifeRecoltingAnt = lifeRecoltingAnt_;
	lifeSoldierAnt = lifeSoldierAnt_;
	MaxNumberResourcesCanCarried =MaxNumberResourcesCanCarried_;
	soldierStrength = soldierStrength_;

	ground = Ground::getInstance(object["Ground"]["length"], object["Ground"]["height"], ConstructAntHillVector);
	Ant * ant = nullptr;
	
	for (unsigned int i = 0; i < numberOfColonies; ++i)
	{
		unsigned int xAntHill = ConstructAntHillVector[i].xAntHill;
	    unsigned int yAntHill = ConstructAntHillVector[i].yAntHill;
		Ant::colony colonyId = ConstructAntHillVector[i].colonyId;
		
		ant = FactoryAnt("QueenAnt", colonyId, xAntHill, yAntHill);
		antList.push_back(ant);
	}
}


/* ---------------------------------------------------------------------------------- */
// Fonction qui réalise les actions de toutes les fourmis présentent dans la simulation
// L'ordre des actions sont :
// 			action de mouvements 
//			actions propres à chaque type de fourmis et 
//			mise à jours  de chaque fourmis ( si elles sont mortes durant le tour ou non )
// et ensuite on affiche le terrain avec le resultat de toutes ces actions 
// Ce processus est répété autant de fois que l'entier donné en argument d'entré
/* ---------------------------------------------------------------------------------- */
void Simulation::runSimu(int n)
{	
	// la liste de fourmis de la simulation contient différents type de fourmis, reines, soldats, récoltrices 
	// cependant la fonction communique qu'avec le type fourmi et tout se fait par polymoprhisme 
	
	displaySimulation(ground->getLengthGround(), ground->getHeightGround(), ground);
	int sys ;
	

	while (n > 0) 
	{
		n--;
		// On brasse le vecteur des fourmis actives
		std::random_shuffle(antList.begin(), antList.end()); 

		// On sépare les for car l'on veut avoir réaliser les déplacements de toutes les fourmis avant les actions 
		// et toutes les actions avant de mettre à jours les fourmis 
		for (unsigned int i = 0; i < antList.size(); i++)
		{
			//permet d'afficher les informations basics communes à toutes les fourmis 
			//antList[i]->antInformations() ; 
			
			// fonctions ovveridée qui donne des informations propre à chaque type de fourmis 	
			//antList[i]->antSpecificsInformations() ;	

			// la fourmi bouge en fonction de ses paramètres et ceux du terrain 
			antList[i]->move();
		}
		for (unsigned int i = 0; i < antList.size(); i++)
		{
			// la fourmi réalise son action
			antList[i]->action();
		}
		for (unsigned int i = 0; i < antList.size(); i++)
		{
			// la fourmi vieillit et meurt si elle n'a plus de point de vie 
			antList[i]->updateAnt();
		}
		// On melange le vecteurs des fourmis


		sys = system("clear");
		displaySimulation(ground->getLengthGround(), ground->getHeightGround(), ground);	
		sys = system("sleep 1");
		sys++;
	}
	std::cout << "Nombre de fourmi = " << antList.size() << std::endl;
	nombreDeFourmis = antList.size();
	exportInfo((char * )"recap.txt");
}


Ant * Simulation::FactoryAnt(std::string antType, Ant::colony colonyId, unsigned int xAntHill, unsigned int yAntHill)
{
	Ant * newAnt = nullptr;
	std::string recoltingAnt = "RecoltingAnt";
	std::string soldierAnt = "SoldierAnt";
	std::string queenAnt = "QueenAnt";
	
	if (antType == recoltingAnt)
	{
		newAnt = new RecoltingAnt(ground, colonyId, xAntHill, yAntHill, lifeRecoltingAnt, MaxNumberResourcesCanCarried, this);
	}
	else 
	{
		if (antType == soldierAnt) 
		{
			newAnt = new SoldierAnt(ground, colonyId, xAntHill, yAntHill, lifeSoldierAnt, soldierStrength, this);
		}
		else
		{
			if (antType == queenAnt) 
			{
				newAnt = new QueenAnt(ground, colonyId, xAntHill, yAntHill, lifeQueenAnt, maxNumberEggs, this);
			}
		}
	}
	return newAnt;
}


/* ---------------------------------------------------------------------------------- */
// Fonction qui ajoute une fourmi à la simulation
// Elle ajoute soit une fourmi récoltrice soit une fourmi soldat 
// La probabilité d'avoir une fourmi recoltrice ou bien une fourmi soldat est définie 
// à l'aide d'une variable globale "birthRateRecoltingAnt"
// On n'ajoute pas de fourmis reines à la simulation ces dernieres sont ajoutées lors 
// du début de la simulation
/* ---------------------------------------------------------------------------------- */
void Simulation::addAntToTheSimulation(Ant::colony colonyId, unsigned int xAntHill, unsigned int yAntHill)
{
	double p = genrand_real2();
	Ant * ant = nullptr;
	
	if (p < birthRateRecoltingAnt) 
	{
		ant = FactoryAnt("RecoltingAnt", colonyId, xAntHill, yAntHill);
	}
	else
	{
		ant = FactoryAnt( "SoldierAnt", colonyId, xAntHill, yAntHill);
	}
	
	// la fourmi s'ajoute elle même sur sa case dans son constructeur  
	
	antList.push_back(ant); // ajoute la fourmi à la simulation 
	
}	



/* ---------------------------------------------------------------------------------- */
// Fonction qui retire une fourmi de la simulation
/* ---------------------------------------------------------------------------------- */
void Simulation::removeAntFromSimulation(Ant * ant)
{
	unsigned int antId = ant->getIdAnt();
	for (unsigned int j = 0; j < antList.size(); j++)
	{
		if (antList[j]->getIdAnt() == antId)
		{
			antList.erase(antList.begin() + j);
		}
	}
}


/* ---------------------------------------------------------------------------------- */
// Fonction qui intialise et renvoie une structure qui contiendra toute les informations
// necessaire à la création d'une colonnie de fourmis 
/* ---------------------------------------------------------------------------------- */
ConstructAntHill Simulation::setAntHill(unsigned int xAntHill, unsigned int yAntHill, Ant::colony colonyId, unsigned int numberRessourcesInAntHill)
{
	ConstructAntHill constructAntHill;
	constructAntHill.xAntHill = xAntHill;
	constructAntHill.yAntHill = yAntHill;
	constructAntHill.colonyId = colonyId;
	constructAntHill.numberRessourcesInAntHill = numberRessourcesInAntHill;
	
	return constructAntHill;
}		
	


void Simulation::exportInfo(char * nomFichier)
{
	std::ofstream fichier(nomFichier, std::ios::trunc);
        
    if(fichier)  // si l'ouverture a réussi
    {
    	fichier << "Nombre de fourmis en vie dans la simulation : " << nombreDeFourmis << std::endl;
        fichier.close();  // on referme le fichier
    }
}
