#include "Display.hpp"
#include "Ant.hpp"
#include "Ground.hpp"
#include "AntHill.hpp"

#include <iostream>
#include <iomanip>


void displaySimulation(unsigned int length_Ground, unsigned int height_Ground, Ground * ground)
{
	Square * square = nullptr;
	Ant * ant = nullptr;
	
	std::cout << "=============== GRILLE DE LA SIMULATION ===============" << std::endl << std::endl;

	for (unsigned int i = 0; i < height_Ground; ++i)
	{
		for (unsigned int j = 0; j < length_Ground; ++j)
		{
			square = ground->getSquareWithCoordinates(j, i);
			
			if (isAntHill(square))
			{
				displayAntHill(square);
			}
			else
			{
				if (!(square->isEmpty()))
				{
					ant = square->getAntsVector()[0];
					displayAnt(ant);
					// On pourrait rajouter une fonction qui donne la colonie avec le plus de fourmi sur une case 
				}
				else
				{
					if (square->numberResourcesAvailable() != 0)
					{
						displaySquare(square);
					}
					else
					{
						std::cout << std::right << std::setw(4) << "-";
					}
				}
			}
		}
		std::cout << std::endl;
	}	
}




void displayAntHill(Square * square)
{ 
	AntHill * antHill = dynamic_cast<AntHill *>(square);
	
	if (antHill != nullptr)
	{
		Ant::colony antHillColonyId = antHill->getAntHillId();
		
		switch (antHillColonyId)
		{
			case (Ant::colony1) :
			{
				std::cout << "\033[1;31m" << std::right << std::setw(4) << "F" << "\033[0m";
			} break;
			
			case (Ant::colony2) :
			{
				std::cout << "\033[1;34m" << std::right << std::setw(4) << "F" << "\033[0m";
			} break;
			
			case (Ant::colony3) :
			{
				std::cout << "\033[1;33m" << std::right << std::setw(4)  << "F" << "\033[0m";
			} break;
			
			case (Ant::colony4) :
			{
				std::cout << "\033[1;35m" << std::right << std::setw(4) << "F" << "\033[0m";
			} break;
		}
	}
}


void displaySquare(Square * square)
{ 
	if (square != nullptr)
	{
		std::cout << std::right << std::setw(4) << square->numberResourcesAvailable();
	}
}


void displayAnt(Ant * ant)
{
	if (ant != nullptr)
	{
		Ant::colony antColonyId = ant->getColonyId();
		Square * square = ant->whatIsMySquare();
		
		switch (antColonyId)
		{
			case (Ant::colony1) :
			{
				std::cout << "\033[1;31m" << std::right << std::setw(4) << square->getAntsVector().size() << "\033[0m";
			} break;
			
			case (Ant::colony2) :
			{
				std::cout << "\033[1;34m" << std::right << std::setw(4) << square->getAntsVector().size() << "\033[0m";
			} break;
			
			case (Ant::colony3) :
			{
				std::cout << "\033[1;33m" << std::right << std::setw(4) << square->getAntsVector().size() << "\033[0m";
			} break;
			
			case (Ant::colony4) :
			{
				std::cout << "\033[1;35m" << std::right << std::setw(4) << square->getAntsVector().size() <<"\033[0m";
			} break;
		}
	}
}